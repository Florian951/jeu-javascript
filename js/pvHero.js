
let pvHero = document.querySelector("#pvHero")

let barrePvHero = document.createElement("div");
barrePvHero.classList.add("progress-bar");
barrePvHero.setAttribute("id" , "barrePvHero")
barrePvHero.setAttribute('role' , 'progressbar')
barrePvHero.setAttribute('style', "width:100%");
barrePvHero.setAttribute("aria-valuenow" ,"100");
barrePvHero.setAttribute("aria-valuemin" ,"0");
barrePvHero.setAttribute("aria-valuemax" ,"100");

pvHero.appendChild(barrePvHero);