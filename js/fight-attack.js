let musiqueEpee = document.querySelector("#buitEpee");
let musiqueMortGarde = document.querySelector("#myAudioMort");
let musiqueFondJeu = document.querySelector("#myAudioFond");
let musiqueMortHero = document.querySelector("#myAudioMortHero");


let potion = document.querySelector("#potion");
let pictureDeadHero = document.querySelector("#hero");
let pictureDeadGarde = document.querySelector("#garde")
let textCombat = document.querySelector("#dialogueCombat");
let gardeBarrePv = document.querySelector("#barrePvGarde");
let heroBarrePv = document.querySelector("#barrePvHero");
let bouton = document.querySelector("#boutons")

let ms = 2500;
let counter = 30;



//Potion.................................................................................................................

potion.addEventListener("click", function () {
if(hero.potion == true){
    hero.takePotion();
    potion = barrePvHero.setAttribute("style", "width:" + hero.pv + "%");
    textCombat.textContent = (`La potion te rends 30 PV !`);
}else{
    alert("tu n'as plus de potion")
}
})

//Attaque HERO............................................................................................................

document.addEventListener("keyup", (event) => {

    musiqueFondJeu.play();

    if (event.which === 32 && garde.pv > 0) {
        hero.attack(garde);

        gardeBarrePv = barrePvGarde.setAttribute("style", "width:" + garde.pv + "%");
        textCombat.textContent = (`tu infliges ${hitHeroRandom} de dégats!`);

        musiqueEpee.play();

        

        if (garde.pv <= 0) {
            musiqueMortGarde.play();
            textCombat.textContent = ("Tu arrives à tuer le Garde");

            let buttonSuite = document.createElement("a");
            buttonSuite.classList.add("btn", "btn-primary", "m-2", "col-12");
            buttonSuite.setAttribute("id", "buttonStart1");
            buttonSuite.setAttribute("href", "bossFinal.html")

            bouton.appendChild(buttonSuite);
            buttonSuite.textContent = "En route pour le chateau!";
            pictureDeadGarde.setAttribute("src", "imagesFinal/gardeMort.png");
            pictureDeadGarde.setAttribute("id", "mortGarde");

        }

       

       
         setTimeout(function () {
                attackGarde()
            }, 800);
        }
        setTimeout(800);

    
})

//Attaque GARDE............................................................................................................

function attackGarde() {

    garde.attack(hero);

    heroBarrePv = barrePvHero.setAttribute("style", "width:" + hero.pv + "%");
    textCombat.textContent = (`Le garde t'infliges ${hitGardeRandom} de dégats!`);
    musiqueEpee.play();

    if (garde.pv <= 0) {
        musiqueMortGarde.play();

    }
    if (hero.pv <= 0) {

        musiqueMortHero.play();
        pictureDeadHero.setAttribute("src", "imagesFinal/mortHero.png");
        pictureDeadHero.setAttribute("id", "heroMort");
        

    }
}

