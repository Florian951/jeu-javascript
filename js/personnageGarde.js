let hitGardeRandom ;

class Garde {
    /**
     * @param {string} name 
     * 
     */
    constructor(name) {

        this.name = name;
        this.pv = 100;
        this.live = true;
        this.dead = 0;
    }

    attack(hero) {
        if (this.pv > 0) {
 /**
  * hit garde sur une base random
  */
            function hitGarde() {
                hitGardeRandom = Math.floor(Math.random() * Math.floor(30));
                return hitGardeRandom;
            }
            hitGarde();
            hero.pv -= hitGardeRandom;

        }
    }
}